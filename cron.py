import time
from datetime import datetime


class Job(object):

    def __init__(self, cmd, job, *args, **kwargs):
        self.allow_any = '*'
        self.cmd = cmd
        self.job = job
        self.args = args
        self.kwargs = kwargs

    def istime(self, t):

        def _istime(val, expr):
            return expr is self.allow_any or not val % int(expr)

        return all(_istime(getattr(t, ['minute', 'hour', 'day', 'month'][i]), _
                           ) for i, _ in enumerate(self.cmd.split()))

    def run(self, c_time):
        if self.istime(c_time):
            self.job(*self.args, **self.kwargs)


class Cron(object):

    def __init__(self):
        self._run = False
        self.queue = []

    def add(self, cmd, job, *args, **kwargs):
        """
        cmd: * * * *
             <minute: 1-59, hour: 0-23, day: 1-31, month: 1-23>
        """
        self.queue.append(Job(cmd, job, *args, **kwargs))
        if not self._run:
            self.run()

    def loop(self):
        while self._run:
            time.sleep(60 - time.time() % 60)

            for _ in self.queue:
                _.run(datetime(*datetime.now().timetuple()[:5]))

    def run(self):
        self._run = True  # explicitly assign True
        self.loop()



if __name__ == '__main__':
    #  12 05 * *
    def job1():
        print 'This will run every 5:12 am every day of every month.'

    #  * * 24 *
    def job2():
        print 'This will run every 24 of every month.'

    #  30 12 10 6
    def job3():
        print 'This will run every 12:30pm on June 10th.'

    cron = Cron()
    jobs = [('12 4 * *', job1), ('* * 24 *', job2), ('30 12 10 6', job3)]
    for _ in jobs:
        cron.add(*_)
