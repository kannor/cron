import unittest
from datetime import datetime, timedelta

from cron import Job, Cron


class JobTestCase(unittest.TestCase):

    def setUp(self):
        self.job = Job(cmd='* * * *', job=lambda: 0)

    def test_istime(self):
        self.assertTrue(self.job.istime(datetime.now()))

        #  Test for 'min * * *'
        now = datetime.now()
        self.job.cmd = '%s * * *' % now.minute
        self.assertTrue(self.job.istime(now))

        # Test for 'min hour * *' with 'min * day *'
        self.job.cmd = '%s %s * *' % (now.minute, now.day)
        self.assertFalse(self.job.istime(now))


class CronTestCase(unittest.TestCase):

    def setUp(self):
        self.cron = Cron()

    def _test_minute(self):

        now = datetime.now()

        def run_every_min():
            self.assertEqual(now + timedelta(minutes=1), datetime.now())
        self.cron.add('* * * *', run_every_min)

    def _test_day(self):

        now = datetime.now()

        def run_every_day():
            self.assertEqual(now + timedelta(day=1), datetime.now())
        self.cron.add('* * 1 *', run_every_day)


if __name__ == '__main__':
    unittest.main()
